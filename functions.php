<?php
/*
 * Custom header support
 */
add_theme_support( 'custom-header' );
 /*
 * Remove Wordpress version
 */
remove_action('wp_head', 'wp_generator');

/*
 * Remove Wordpress Manifest url 
 */
remove_action( 'wp_head', 'wlwmanifest_link');

/*
 * Remove Wordpress RSD link
 */
remove_action ('wp_head', 'rsd_link');

/*
 * Add theme support
 */
add_theme_support( 'post-thumbnails' );


/*
 * Remove admin sections 
 */
function remove_menus(){
  remove_menu_page( 'jetpack' );                    //Jetpack* 
  remove_menu_page( 'edit-comments.php' );          //Comments
}
add_action( 'admin_menu', 'remove_menus' );

 /*
 * Disable Emojicons
 */
function disable_wp_emojicons() {
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );

/*
 * This theme uses wp_nav_menu() in one location.
 */
register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'bgfx' ),
) );

// Register Custom Navigation Walker
require_once('libs/wp_bootstrap_navwalker.php');

/**
 * Proper way to enqueue scripts and styles
 */
function bgfx_amuze_scripts() {
    wp_enqueue_style( 'style-name', get_stylesheet_uri() );
    wp_enqueue_script( 'theme', get_template_directory_uri() . '/js/theme.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'bgfx_amuze_scripts' );

/**
 * Enqueue Styles
 */
if (  !function_exists('wp_enqueue_styles')) {
    function wp_enqueue_styles() {
        wp_register_style('theme' , get_template_directory_uri() . '/css/theme.css', array(), '1.0', 'all' );
        wp_enqueue_style( 'theme');
    }
}
add_action('wp_enqueue_scripts' , 'wp_enqueue_styles');


/**
 * Generate team post type
 */
// Register Custom Post Type
function team_post_type() {

  $labels = array(
    'name'                  => _x( 'Teams', 'Post Type General Name', 'amuze' ),
    'singular_name'         => _x( 'Team', 'Post Type Singular Name', 'amuze' ),
    'menu_name'             => __( 'Team', 'amuze' ),
    'name_admin_bar'        => __( 'Team', 'amuze' ),
    'archives'              => __( 'Item Archives', 'amuze' ),
    'attributes'            => __( 'Item Attributes', 'amuze' ),
    'parent_item_colon'     => __( 'Parent Item:', 'amuze' ),
    'all_items'             => __( 'All Items', 'amuze' ),
    'add_new_item'          => __( 'Add New Item', 'amuze' ),
    'add_new'               => __( 'Toevoegen', 'amuze' ),
    'new_item'              => __( 'New Item', 'amuze' ),
    'edit_item'             => __( 'Edit Item', 'amuze' ),
    'update_item'           => __( 'Update Item', 'amuze' ),
    'view_item'             => __( 'View Item', 'amuze' ),
    'view_items'            => __( 'View Items', 'amuze' ),
    'search_items'          => __( 'Search Item', 'amuze' ),
    'not_found'             => __( 'Not found', 'amuze' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'amuze' ),
    'featured_image'        => __( 'Featured Image', 'amuze' ),
    'set_featured_image'    => __( 'Set featured image', 'amuze' ),
    'remove_featured_image' => __( 'Remove featured image', 'amuze' ),
    'use_featured_image'    => __( 'Use as featured image', 'amuze' ),
    'insert_into_item'      => __( 'Insert into item', 'amuze' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'amuze' ),
    'items_list'            => __( 'Items list', 'amuze' ),
    'items_list_navigation' => __( 'Items list navigation', 'amuze' ),
    'filter_items_list'     => __( 'Filter items list', 'amuze' ),
  );
  $args = array(
    'label'                 => __( 'Team', 'amuze' ),
    'description'           => __( 'Amuze team post type', 'amuze' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'thumbnail', 'custom-fields', ),
    'taxonomies'            => array( 'category', 'post_tag' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => false,   
    'exclude_from_search'   => true,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'show_in_rest'          => true,
  );
  register_post_type( 'amuze_team', $args );

}
add_action( 'init', 'team_post_type', 0 );

/**
 * Register our sidebars and widgetized areas.
 */
function bgfx_widgets_init() {
  register_sidebar( array(
    'name' => 'Footer',
    'id' => 'footer-blocks',
    'description' => 'Footer blcoks',
    'before_widget' => '<div id="%1$s" class="col-md-3 widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
  ));
}
add_action( 'widgets_init', 'bgfx_widgets_init' );


/**
 * Register food menu custom post type and link ACF field.
 */
// Register Custom Post Type
function food_menus() {

  $labels = array(
    'name'                  => _x( 'Menus', 'Post Type General Name', 'amuze' ),
    'singular_name'         => _x( 'Menu', 'Post Type Singular Name', 'amuze' ),
    'menu_name'             => __( 'Food menus', 'amuze' ),
    'name_admin_bar'        => __( 'Food menu', 'amuze' ),
    'archives'              => __( 'Item Archives', 'amuze' ),
    'attributes'            => __( 'Item Attributes', 'amuze' ),
    'parent_item_colon'     => __( 'Parent Item:', 'amuze' ),
    'all_items'             => __( 'All Items', 'amuze' ),
    'add_new_item'          => __( 'Add New Item', 'amuze' ),
    'add_new'               => __( 'Add New menu', 'amuze' ),
    'new_item'              => __( 'New menu', 'amuze' ),
    'edit_item'             => __( 'Edit menu', 'amuze' ),
    'update_item'           => __( 'Update menu', 'amuze' ),
    'view_item'             => __( 'View Item', 'amuze' ),
    'view_items'            => __( 'View Items', 'amuze' ),
    'search_items'          => __( 'Search Item', 'amuze' ),
    'not_found'             => __( 'Not found', 'amuze' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'amuze' ),
    'featured_image'        => __( 'Featured Image', 'amuze' ),
    'set_featured_image'    => __( 'Set featured image', 'amuze' ),
    'remove_featured_image' => __( 'Remove featured image', 'amuze' ),
    'use_featured_image'    => __( 'Use as featured image', 'amuze' ),
    'insert_into_item'      => __( 'Insert into item', 'amuze' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'amuze' ),
    'items_list'            => __( 'Items list', 'amuze' ),
    'items_list_navigation' => __( 'Items list navigation', 'amuze' ),
    'filter_items_list'     => __( 'Filter items list', 'amuze' ),
  );
  $args = array(
    'label'                 => __( 'Menu', 'amuze' ),
    'description'           => __( 'Food menus', 'amuze' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'excerpt', 'thumbnail', 'custom-fields', 'page-attributes'),
    'taxonomies'            => false,
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,    
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'food_menu', $args );

}
add_action( 'init', 'food_menus', 0 );

/**
 * Custom Poste Type : Gangen
 */
// Register Custom Post Type
function gangen() {

    $labels = array(
        'name'                  => _x( 'Gangen', 'Post Type General Name', 'amuze' ),
        'singular_name'         => _x( 'Gang', 'Post Type Singular Name', 'amuze' ),
        'menu_name'             => __( 'Gangen', 'amuze' ),
        'name_admin_bar'        => __( 'Post Type', 'amuze' ),
        'archives'              => __( 'Item Archives', 'amuze' ),
        'attributes'            => __( 'Item Attributes', 'amuze' ),
        'parent_item_colon'     => __( 'Parent Item:', 'amuze' ),
        'all_items'             => __( 'All Items', 'amuze' ),
        'add_new_item'          => __( 'Add New Item', 'amuze' ),
        'add_new'               => __( 'Gang toevoegen', 'amuze' ),
        'new_item'              => __( 'New Item', 'amuze' ),
        'edit_item'             => __( 'Edit Item', 'amuze' ),
        'update_item'           => __( 'Update Item', 'amuze' ),
        'view_item'             => __( 'View Item', 'amuze' ),
        'view_items'            => __( 'View Items', 'amuze' ),
        'search_items'          => __( 'Search Item', 'amuze' ),
        'not_found'             => __( 'Not found', 'amuze' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'amuze' ),
        'featured_image'        => __( 'Featured Image', 'amuze' ),
        'set_featured_image'    => __( 'Set featured image', 'amuze' ),
        'remove_featured_image' => __( 'Remove featured image', 'amuze' ),
        'use_featured_image'    => __( 'Use as featured image', 'amuze' ),
        'insert_into_item'      => __( 'Insert into item', 'amuze' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'amuze' ),
        'items_list'            => __( 'Items list', 'amuze' ),
        'items_list_navigation' => __( 'Items list navigation', 'amuze' ),
        'filter_items_list'     => __( 'Filter items list', 'amuze' ),
    );
    $args = array(
        'label'                 => __( 'Gang', 'amuze' ),
        'description'           => __( 'Gangen', 'amuze' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'custom-fields', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,        
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'gangen', $args );

}
add_action( 'init', 'gangen', 0 );
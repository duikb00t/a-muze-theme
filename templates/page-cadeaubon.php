<?php
/**
 *	Template Name: Cadeaubon
 */
?>
 <div class="slogan">
    <span>
      <?php $key="slogan"; echo get_post_meta($post->ID, $key, true); ?>
    </span>
</div>
<div class="divider"></div>
	<section class="bg-2">
	    <div class="col-sm-10 col-sm-offset-1 text-center">
            <div id="footer_slogan">
	         <h4>BUYA</h4>
	         <h3>Present</h3>
           </div>
	    </div>
	</section>
<div class="divider"></div>
<div class="container-fluid">
  <section>
    <div class="col-md-5 col-md-offset-4">
      <?php the_content(); ?>
    </div>
  </section>
  <div class="clear"></div>
</div>
<div id="kadobon">
  <a href="#ReserveerNu" id="cadeaubon_url" onclick="ReserveerNu('NL')">Cadeaubon</a>
  <script>
   function ReserveerNu(strLanguage) {
    $.fancybox(
    {
        'hideOnContentClick': false,
        'autoScale': false,
        'width': 700,
        'height': 650,
        'type': 'iframe',
        'href': 'https://www.resengo.com/Code/calendar/calendar_small.asp?CID=1874&AID=1&VS=0&CACID=1&LC=' + strLanguage
    });
   }
</script>

</div>

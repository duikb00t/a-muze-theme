<?php
// Template Name: Team
?>
<?php  $query = new WP_Query( array( 'post_type' => 'amuze_team', 'paged' => $paged ) ); ?>

<?php if ( $query->have_posts() ) : ?>
<div id="team" class="container-fluid">
  <div class="slogan row">
    <span>
      <?php $key="slogan"; echo get_post_meta($post->ID, $key, true); ?>
    </span>
  </div>
  <div class="row">
    <?php while ( $query->have_posts() ) : $query->the_post(); ?>	
      <div class="team_member col-md-3 col-sm-6 col-xs-6">
        <figure>
          <img class="img-responsive" src="
          <?php
          $thumb_id = get_post_thumbnail_id();
          $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
          echo $thumb_url[0];
          ?>
          " alt="Team Person">
        </figure>
      <span><?php the_title(); ?></span>
    	<?php $function = get_post_meta( get_the_ID(), 'team_function', true); ?>
    	<span><?php echo !empty($function) ? $function : 'nee'; ?></span>
		</div>
	  <?php endwhile; wp_reset_postdata(); ?>
	
  <div id="vacatures">
    <div id="join_team" class="col-md-12 col-xs-12">
      <a href="mailto:info@a-muze.be">Wij zijn steeds op zoek naar gemotiveerde medewerks</a>
    </div>
    
    <div class="col-md-6 col-xs-12">KELNERS (WEEKENDWERK) </div>
    <div class="col-md-6 col-xs-12">AFWASSERS (WEEKENDWERK)</div>
  </div>

  <div class="col-md-5 col-md-offset-4">
    <div class="divider"></div>
    <?php the_content(); ?>
  </div>

  </div>
</div>
<?php else : ?>
<?php endif; ?>
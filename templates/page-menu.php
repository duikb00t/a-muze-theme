<?php
/**
 * Template name: Menu
 */
$posts = get_posts(array(
	'posts_per_page'	=> -1,
	'post_type'			=> 'food_menu'
));
?>
<!-- MENUS -->
<?php if( $posts ): ?>

<div class="container-fluid" id="menu_blocks">
	<div class="row">
	<?php
	// Menu Counter
	$i =1; 
	?>
	<?php foreach( $posts as $post ):  setup_postdata( $post ); ?>
		<?php
		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
		$image_alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true);
		?>

		<div class="col-md-4 col-xs-12">
			<div class="row">
				<a class="cover_url" href="#" data-menuid="<?php echo $i; ?>">
					<div class="cover">
						<span style="background-image:url('<?php echo $thumb_url[0];?>')"></span>
					</div>
					<h4><?php echo the_title(); ?></h4>
				</a>
			</div>
		</div>

	<?php 
	$i++; // Menu counter increments
	endforeach; $i=1; // reset ?>
	</div>
</div>

<!-- SUB-MENUS -->

<?php $i=1; ?>
<?php foreach( $posts as $post ):  setup_postdata( $post ); ?>
	<div class="menu_detail container-fluid" data-menuid="<?php echo $i; ?>">
		<div class="container">
	<?php if( have_rows('menu_naam') ):
	    while ( have_rows('menu_naam') ) : the_row();
	        echo '<h2 style="color:white; text-align:center;">' . get_sub_field('datum_veld') . $i . '</h2>';
	    endwhile;
		else :
	endif;
	?>
	<?php if( have_rows('menu_naam') ):
	    while ( have_rows('menu_naam') ) : the_row();
	    	echo '<div class="menu_submenu_boxes">';
	        if( have_rows('datum') ):
	        	while ( have_rows('datum') ) : the_row();
	        		echo '<div class="submenu col-md-2">';
	        		echo '<h5>' . get_sub_field('veld_1') . '</h5>';
					echo the_sub_field('omschrijving');
	        		echo '</div>';
	        	endwhile;
    		endif;
    		echo '</div>';
	    endwhile;
		else :
	endif;
	?>
	</div>

<?php $i++; ?>
</div>
<?php endforeach; $i=1; ?>
<?php wp_reset_postdata(); ?>


<?php
$posts = get_posts(array(
	'posts_per_page'	=> -1,
	'post_type'			=> 'gangen',
	'order'				=>	'asc'
));
?>
<!-- Default menu picker -->
<div class="container-fluid" id="gangen">
	<div class="row">
		<?php $gid = 1; ?>
		<?php foreach( $posts as $post ):  setup_postdata( $post ); ?>
    	<div data-gangid="<?php echo $gid; ?>" class="col-md-3 col-xs-12">
    		<h4><?php the_title(); ?></h4>
    		<?php the_content(); ?>
    	</div>
    	<?php $gid++; ?>
    	<?php endforeach; ?>	
	</div>
</div>	
<?php endif; ?>





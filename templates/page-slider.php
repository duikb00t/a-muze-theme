<?php
/**
 *	Template Name: Slider
 */
$images = get_field('imageslider');
if( $images ): ?>
<div id="photo-slider-wrapper" class="container-fluid">
    <div id="photo-slider">
    	<?php foreach( $images as $image ): ?>
			<div class="slide">
				<a href="">
					<span style="background-image:url('<?php echo $image['sizes']['large']; ?>');"></span>
				</a>
			</div>
		<?php endforeach; ?>
	</div>
</div>
<?php endif; ?>
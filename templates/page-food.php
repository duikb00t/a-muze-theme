<div class="container">
  <section>
    <div class="col-2">
      <h2 class="over-ons"><?php the_title(); ?></h2>
      <?php the_content(); ?>
    </div>
  </section>
  <div class="clear"></div>
</div>
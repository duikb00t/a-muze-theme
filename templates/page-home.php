<?php
/**
 * Template name: Menu
 */
?>
 <div id="intro_content" class="container-fluid">
 	<section>
	    <div class="col-md-5 col-md-offset-4">
		<?php the_content(); ?>
	  	</div>
  	</section>
	<div class="clear"></div>
		<div class="slogan" style="border-top:1px solid #999999">
		<span>
		<?php $key="slogan"; echo get_post_meta($post->ID, $key, true); ?>
		</span>
	</div>

		<?php
		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
		?>
		<div class="divider"></div>
		<section class="bg-3" style="background:url('<?php echo $thumb_url[0]; ?>') no-repeat top center fixed;">
		    <div class="col-sm-10 col-sm-offset-1 text-center">
		    	<div id="home_slogan">
		         	<h3>Tastefull</h3>
		        	<h4>Experience</h4>
	        	 </div>
	    	</div>
		</section>

</div>



  
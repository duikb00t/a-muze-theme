$(document).ready(function() {

    /*
     * Casual nice scrollTo Behaviour
     */
     $(function() {
      $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html, body').animate({
              scrollTop: target.offset().top - 140
            }, 1000);
            return false;
          }
        }
      });
    });

    /*
     * Slick slider
     */
    $("#photo-slider").slick({
        dots: true,
        slidesPerRow: 4,
        rows: 2,
        responsive: [
        {
          breakpoint: 478,
          settings: {
            slidesPerRow: 2,
            rows: 2,
          }
        }
      ]
    });

    /**
    * Parallax 
    */
    $('.bg-2').parallax({
        speed : 0.25
    });

    /**
    * Menu Systeem
    **/
    var $menus = $('.menu_detail').hide();

    $('.cover_url').on('click', function(e) {
      e.preventDefault();
      $menus.hide();

      /**
      * Menu systeem periodes hover
      **/
      console.log($(this));
      $(this).find('.menu_submenu_boxes').hide()
      $(this).first().show();



      var clickedID = $(this).data('menuid');
      $('.menu_detail').filter('[data-menuid="' + clickedID + '"]').slideToggle();
    })



    /**
    * Gangen systeem hover
    **/
    $( "#gangen div div" ).hover(
    function() {

      var hoveredGang = $(this).data('gangid');
      
      switch (hoveredGang) {
        case 1:
        $('.menu_detail .submenu:lt(3)').addClass('activeSubmenu');
        break;

        case 2:
        $('.menu_detail .submenu:lt(4)').addClass('activeSubmenu');
        break;

        case 3:
        $('.menu_detail .submenu:lt(5)').addClass('activeSubmenu');
        break;

        case 4:
        $('.menu_detail .submenu:lt(6)').addClass('activeSubmenu');
        break;
      }
    }, function() {
      $('.menu_detail .submenu').removeClass('activeSubmenu');
    }
  );
});
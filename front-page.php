<?php
/**
 * The front page template.
 */
?>
<?php get_header(); ?>
<?php query_posts('post_type=page&orderby=menu_order&order=ASC'); ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		 <?php get_template_part('templates/page', get_post_field('post_name', $post->ID )); ?> 
    <?php endwhile; endif; ?>
<?php get_footer(); ?>

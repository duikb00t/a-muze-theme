	<footer>
		<div class="divider"></div>
			<section class="bg-2">
			    <div class="col-sm-10 col-sm-offset-1 text-center">
			    	<div id="footer_slogan">
				         <h4>RECEIVE</h4>
				         <h3>The menu</h3>
			         </div>
			    </div>
			</section>
		<div id="contact">
		<?php echo FrmFormsController::get_form_shortcode( array( 'id' => 2, 'title' => false, 'description' => false ) ); ?>
		</div>

		<div class="slogan">
			<span>TRUE TEAMS ARE MADE WHEN YOU PUT ASIDE INDIVIDUAL<br/>WANTS FOR COLLECTIVE GOODS!</span>
		</div>

		<div id="footer" class="container-fluid">
			<div class="row">
				<?php if(is_active_sidebar('footer-blocks')){ dynamic_sidebar('footer-blocks'); } ?>
			</div>

	      	<div id="social" class="row">
		        <div class="col-md-4">
		          <a href="https://www.facebook.com/AMuzeOpwijk/" target="_BLANK">Facebook</a>
		        </div>
		        <div class="col-md-4">
		          <a href="https://www.instagram.com/explore/locations/281583350/amuze-restaurant/" target="_BLANK">Instagram</a>
		        </div>
		        <div class="col-md-4">
		          <a href="#">Nieuwsbrief</a>
		        </div>
	        </div>
    	</div>
	</footer>
	
	<?php wp_footer(); ?>
  </body>
</html>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<!-- B-Art graphics Webdesign -->
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title><?php wp_title(); ?></title>
		<link href="https://fonts.googleapis.com/css?family=Kristi" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
		
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		  ga('create', 'UA-89120885-1', 'auto');
		  ga('send', 'pageview');
	  	</script>

		<?php wp_head(); ?>
	</head>
	<body id="main_top"<?php body_class(); ?>>
		<header>
			<nav class="navbar navbar-default" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand animated fadeInLeft" href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a>
	</div>
	<?php
	wp_nav_menu( array(
        'menu'              => 'primary',
        'theme_location'    => 'primary',
        'depth'             => 2,
        'container'         => 'div',
        'container_class'   => 'collapse navbar-collapse',
		'container_id'      => 'bs-example-navbar-collapse-1',
		'menu_class'        => 'nav navbar-nav  navbar-right',
		'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
		'walker'            => new wp_bootstrap_navwalker())
	);
	?>
	 </div>
</nav>
<div id="head_image">
	<span style="background-image: url('<?php header_image(); ?>'); background-attachment: fixed;"></span>
		<div class="container">
			<div class="animated fadeInDown" id="logo_wrapper">
				<em>Be <a href="#resengo-booknow">Reserveren</a></em>
				<em>A'Muzed</em>
			</div>	
		</div>
	</div>
	<div class='icon-scroll'><div/>
</header>


var gulp 	= require('gulp'),
sass 		= require('gulp-sass'),
jshint 		= require('gulp-jshint'),
concat 		= require('gulp-concat'),
plumber 	= require('gulp-plumber'),
notify 		= require('gulp-notify'),
include   	= require("gulp-include"),
livereload 	= require('gulp-livereload');


var paths = {
	styles: './css/src/*.scss',
	scripts: './js/src/*.js',
	dist: 'dist/'
};
	
// Sass
gulp.task('sass', function () {
	gulp.src(paths.styles)
	.pipe(plumber(plumberErrorHandler))
	.pipe(sass({
		includePaths: require('node-normalize-scss').includePaths
	}))
	.pipe(gulp.dest('./css'))
	.pipe(livereload());
});

// Javascript stuff
gulp.task("js", function() {
  	gulp.src([
      "./node_modules/jquery/dist/jquery.js",
  		"./node_modules/jquery-fancybox/source/js/jquery.fancybox.js",
  		"./node_modules/slick-carousel/slick/slick.min.js",
  		"./js/src/jquery.bxslider.min.js",
  		"./js/src/jquery.parallax.js",
  		"./node_modules/bootstrap-sass/assets/javascripts/bootstrap.js",
  		paths.scripts,
	])

    .pipe(concat('theme.js'))
    .pipe(gulp.dest("js"))
    .pipe(livereload());
});

// Watch it! 
gulp.task('watch', function() {
  livereload.listen();
  gulp.watch(paths.styles, ['sass']);
  gulp.watch(paths.scripts, ['js']);
});

// Error Handling
var plumberErrorHandler = { errorHandler: notify.onError({
    title: 'Gulp',
    message: 'Error: <%= error.message %>'
  })
};

// Merge tasks to the default task
gulp.task('default', [
	'sass',
	'js',
	'watch'
]);
